import logo from './logo.svg';
import { useState, useRef} from 'react';
import './App.css';

function App() {

  const MOTDEPASSE = 'a';
  const [ currentValue, setCurrentValue ] = useState('');
  const [ loggedIn, setLoginStatus] = useState(false);
  const inputEl = useRef(null);

  const CheckForMdp = () => {
    
    if(currentValue === MOTDEPASSE){
      setLoginStatus(true);
    }else{
      inputEl.value = '';
    }
      

      
  }
  
  return (
    <div className="App">
      {(!loggedIn) && 
        <div>
          <h1>Mot de passe ultra secret et sécurisé</h1>
          <form>
            <input ref={inputEl} onChange={(e) => { setCurrentValue(e.target.value) }}/>
            <button onClick={() => CheckForMdp()}>Login</button>
          </form>
        </div>
      }

      {loggedIn && <p>You are logged in ! Well played Hacker B)</p>}
    </div>
  );
}

export default App;
