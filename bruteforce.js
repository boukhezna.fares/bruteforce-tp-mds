const puppeteer = require('puppeteer');

var arr = [];
var index = 0;

let success = false;
let passwordFound = "";
let prevStr;

var pageInstance = null;

async function recursive(istr,curstr,count) {
  count--;
  for(var i=0; i<istr.length; i++) {
    var str = curstr + istr.charAt(i);
    if(count>0) {
      await recursive(istr,str,count);
    }
    else {
      
      console.log("checking for success...");
      let successMessage =(await pageInstance.$('p')) || "";

      if(successMessage){
        success = true;
        passwordFound = prevStr;
        break;
      }

      

      console.log("typing...");
      await pageInstance.type('input', str, {delay: 200});
      await pageInstance.waitForTimeout(100);
      console.log("clicking...");
      await pageInstance.click('button');
      await pageInstance.waitForTimeout(100);

        
        // or they are in the array here
        arr[index++] = str;
        prevStr = str;
    }
  }
}

let utfString = "";
for(let i=33; i <= 126; i++){
    utfString += String.fromCharCode(i);
}

async function enumerate(str, n) {
  for(var i=0;i<n;i++) {
    await recursive(str,"",i+1);

    if(success)
      console.log("mot de passe trouvé :" , passwordFound);
      break;
  }
} 

(async () => {
  const browser = await puppeteer.launch({ headless: false});
  pageInstance = await browser.newPage();
  await pageInstance.goto("http://localhost:3000");
  await new Promise(f => setTimeout(f, 1000));


  await enumerate(utfString, 3);

})();
